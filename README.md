Syntax highlighting with good look and cool JavaScript-effects if you move your mouse over the code. You can turn on
several different options.

![Picture of the recipe](http://www.pmwiki.org/pmwiki/uploads/Cookbook/syntaxlove.png)


# Questions answered by this recipe

How can I display highlighted code of several languages on my wiki?


# Description

This recipe gives you the markup for embedding source code in your Wiki with syntax highlighting. Just follow the
instruction of the installation.


# Installation

- download from [source][source] or via git `https://bitbucket.org/wikimatze/pmwiki-syntaxlove-recipe.git`
- make the following copy-tasks:
  - put `syntaxlove.php` to your cookbook directory (`cp pmwiki-syntaxlove-recipe/cookbook/syntaxlove.php cookbook/`)
  - put `syntaxlove` directory in your public directory (normally *pub/*) (`cp -R pmwiki-syntaxlove-recipe/syntaxlove/ pub/`)
- add `include_once("$FarmD/cookbook/syntaxlove.php);"` after including the following in your *config.php*

    $Array_Syntaxlove =
           array("Bash"   => "0", "CSharp" => "0", "Css"     => "0", "Delphi" => "0",
                 "Groovy" => "0", "Java"   => "1", "JScript" => "0", "Perl"   => "1",
                 "Php"    => "1", "Plain"  => "0", "Python" => "1",  "Ruby"   => "1",
                 "Scala"  => "0", "Sql"    => "0", "Vb"     => "0",  "Xml"    => "0");


# Usage

- use the following markup on your wiki-pages: `(:codestart ruby:) ... (:codeend:)`
- it is also possible to highlight certain lines, to disable wrapping and many other options: `(:codestart ruby
  highlight='[2,4]' wrap='false':) ... (:codeend:)`
- other options are:
    - `collapse` (allows you to hide the code and display and 'expand' code snippet )
    - `gutter` (turn line number on or off)
    - `highlight` (mark special line)
    - `toolbar` (enable or disable the toolbar)`
- to switch to another language just replace ruby with one of the following languages: `bash, csharp, css, cpp, elphi, groovy, java, jscript (JavaScript), perl, php, plain, python, ruby, scala, sql, vb, xml`


# Configuration

There are the following styles: *shThemeDefault*, *shThemeDjango* ... (more can be found under *syntaxlove/css*). To
define a new style just go in the *syntaxlove.php* file and configure the `$syntax_style` variable. Don't hesitate to
develop your own style.


To turn on or turn off a language just set 1 (for on) or 0 (for off) in the
`$Array_Syntaxlove`-variable.


# Notes

Credits goes to [Alex Gorbatchev][alex].


# See Also

* [SourceBlock][sourceblock]
* [Beautifier][beautifier]


# Contributors

[MatthiasGünther](http://www.pmwiki.org/wiki/Profiles/MatthiasGünther)


# Comments/Feedback

See [Syntaxlove-Talk][talk] - your comments are welcome here!


# Contact

Feature request, bugs, questions, and so on can be send to <matthias.guenther@wikimatze.de>. If you enjoy
the script please leave your comment under [Syntaxlove Users][Syntaxlove Users].


# License

This software is licensed under the [MIT license] [mit].

© 2011-2017 Matthias Guenther <matthias@wikimatze.de>.

[Syntaxlove Users]: http://www.pmwiki.org/wiki/Cookbook/Syntaxlove-Users/
[alex]: http://alexgorbatchev.com/
[beautifier]: http://www.pmwiki.org/wiki/Cookbook/Beautifier/
[mit]: http://en.wikipedia.org/wiki/MIT_License
[source]: https://bitbucket.org/wikimatze/pmwiki-syntaxlove-recipe/overview/
[sourceblock]: http://www.pmwiki.org/wiki/Cookbook/SourceBlock/
[talk]: http://www.pmwiki.org/wiki/Cookbook/Syntaxlove-Talk/

/* vim: set ts=2 sw=2 textwidth=120: */
